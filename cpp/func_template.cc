
#include <algorithm>
#include <iostream>

template <typename T>
int compare(const T &a, const T &b)
{
    if (std::less<T>()(a, b))
    {
        return -1;
    }
    else if (std::less<T>()(b, a))
    {
        return 1;
    }
    return 0;
}

int main()
{
    std::cout << compare(12.3, 34.0) << "\n";
    return 0;
}