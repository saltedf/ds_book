#include <iostream>
#include <memory>
#include <utility>
#include <vector>
#include <string>
#include <algorithm>

template <typename T>
class Blob
{
public:
    typedef T value_type;
    typedef typename std::vector<T>::size_type size_type;

    Blob()
        : data_(std::make_shared<std::vector<T>>()) {}
    Blob(std::initializer_list<T> list)
        : data_(std::make_shared<std::vector<T>>(list)) {}

    size_type size() const
    {
        return data_->size();
    }
    bool empty() const
    {
        return data_->empty();
    }

    void push_back(const T &val)
    {
        data_->push_back(val);
    }
    void push_back(T &&val)
    {
        data_->push_back(std::move(val));
    }

    void pop_back()
    {
        data_->pop_back();
    }

    T &back() const
    {
        return data_->back();
    }
    T &operator[](size_type i) const
    {
        checkIndex(i, "out of range!");
        return data_->at(i);
    }

private:
    std::shared_ptr<std::vector<T>> data_;

private:
    void checkIndex(size_type i, const std::string &msg) const
    {
        if (i < 0 || i >= data_->size())
        {
            throw std::out_of_range(msg);
        }
    }
};

int main()
{
    Blob<int> b = {2, 4, 5, 43};
    std::cout << b.back() << "\n";
}