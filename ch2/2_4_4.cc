
#include <iostream>
#include <vector>


template <typename Comparable>
int BinarySearch( const std::vector<Comparable> & arr ,const Comparable& x){

    typename std::vector<Comparable>::size_type low{0};
    typename  std::vector<Comparable>::size_type high{arr.size() -1 } ;

    auto mid = (low + high) /2;
    
    while(low<=high) {
        auto mid_val = arr[mid] ;
        if(x < mid_val){
            high = mid ;
            mid = (low+high)/2;
        } else if(x>mid_val)  {
            low = mid+1;
            mid = (low + high)/2;
        } else {
            return mid;
        }
    }
    return -1 ;
}



        



int main() {

    


    std::vector<int> a = { 43,3,6,89 };
    std::cout <<a[ BinarySearch(a, 89 )] ;

    std::cout << "\n" ;







}
