#include <vector>
#include <iostream>


template <typename Object>
inline const Object& Max3(const Object& a ,const Object& b ,const Object& c) {
    auto tmp = b > c ? b:c;
    return  a > tmp ?a: tmp ;
}

int MaxSubSum1(const std::vector<int> &a ){

    auto arr_size = a.size() ;

    int max_sum{};
    for(decltype(a.size()) i= 0 ;i <arr_size ; ++i ) {

        for(decltype(a.size()) j = i; j< arr_size; ++j) {
            int cur_sum{};
            for(auto k = i;k <= j ;++k ){
                cur_sum += a[k];
            }

            if(cur_sum> max_sum){
                max_sum = cur_sum ;
            }

        }
    }

    return max_sum;
}


int MaxSubSum2(const std::vector<int> &a) {
    auto arr_size= a.size();
    int max_sum{};

    for(decltype(a.size()) i =0 ;i<arr_size ;++i ){

        int cur_sum{};
        for(auto j = i ;j<arr_size ;++j ) {

            cur_sum+= a[j];
            if(cur_sum > max_sum) {
                max_sum  = cur_sum ;
            }
        }
    }

    return max_sum;
}


//分治解法
//整个数组分成两部分，最大子序列和 = 
//左半部分中包含最后一个元素的最大子序列和
//右半部分中包含首个元素的最大子序列和
//或者 左半部分的最大子序列和
//或 右半部分的最大子序列和
int MaxSubSum3(const std::vector<int> &a ,int left ,int right) {


    if(left == right) {
        if(a[left] >= 0 ){
            return a[left];
        } else {
            return 0;
        }
    }
    
    int center = (left+ right)/2 ;
    
    int max_left_sum = MaxSubSum3(a,left ,center);
    int max_right_sum = MaxSubSum3(a,center+1 ,right);

    int max_leftedge_sum{};
    int leftedge_sum{};

    for(int i = center;i>= left ;--i ) {
        leftedge_sum += a[i];
        if(leftedge_sum > max_leftedge_sum ){
            max_leftedge_sum  = leftedge_sum ;
        }
    }

    int max_rightedge_sum{};
    int rightedge_sum{};

    for(int i = center+1 ;i<=right ;++i ){
        rightedge_sum += a[i];
        if(rightedge_sum > max_rightedge_sum ) {
            max_rightedge_sum = rightedge_sum ;
        }
    }
    return  Max3(max_left_sum ,max_right_sum  , max_leftedge_sum + max_rightedge_sum ) ;

}






int main() {


    std::vector<int> arr { 90, -100, 50,55,3 ,5};
    
    std::cout <<MaxSubSum1(arr) << "\n" ;

    std::cout<<MaxSubSum2(arr) << "\n";

    std::cout << MaxSubSum3(arr ,0,arr.size()-1) << "\n" ;

    return 0;    
}



