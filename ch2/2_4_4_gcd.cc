
#include <iostream>

long long Gcd(long long m , long long n){

    while(n!= 0 ) {
        long long remain = m%n;
        m = n ;
        n = remain;
    }
    return m;

}


int main(){


    std::cout <<  Gcd(100 ,32) << "\n" ;
    

}
