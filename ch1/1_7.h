#ifndef DSBOOK_MATRIX_H_
#define DSBOOK_MATRIX_H_
#include <vector>


template <typename Object>
class Matrix {
    public:
        explicit Matrix(int rows,int cols)
            : array_(rows) {
                for(auto &cur_row : array_){
                    cur_row.resize(cols) ;
                }
            }

        Matrix(const std::vector<std::vector<Object> >& arr) 
            :array_(arr) {}

        Matrix(std::vector<std::vector<Object>>&& arr) 
            : array_(std::move(arr) ) {}

        const std::vector<Object>& operator[] (int row) const {
            return array_[row];
        }

        std::vector<Object>& operator[] (int row) {
            return array_[row];
        }


        int num_rows() const {
            return array_.size();
        }

        int num_cols() const {
            return array_[0].size();
        }
        





    private:
        std::vector<std::vector<Object>> array_;


};






            


#endif //DSBOOK_MATRIX_H_

