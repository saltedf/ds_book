
#include <iostream>
#include <vector>
#include <string>


template <typename Object> 
class MemoryCell {
    private:
        Object stored_value_;

    public :
        explicit MemoryCell(const Object& init_val = Object{} ) 
            :stored_value_(init_val) {}

        MemoryCell(const MemoryCell<Object>& rhs) = delete;
        MemoryCell<Object>& operator=(const MemoryCell<Object>& rhs) = delete;

        MemoryCell(MemoryCell<Object>&& rhs) = delete;
        MemoryCell<Object>& operator=( MemoryCell<Object>&& rhs) = delete;

        const Object& read() const {
            return stored_value_ ;
        }

        void write(const Object& x) {
            stored_value_ = x;
        }

};



int main() {


    MemoryCell<int>  int_cell{};

    int_cell.write(100);
    std::cout << int_cell.read() << "\n" ;



}


