#ifndef IntCell_H_

#define IntCell_H_

class IntCell 
{

    public :
        explicit IntCell(int initval = 0);
        int read() const ;
        void write(int x) ;


    private:
        int stored_val_;



};



#endif

