
cmake_minimum_required(VERSION 3.10)

set(CMAKE_CXX_STRAND 14) 

project(ch1)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON )
set(CMAKE_CXX_FLAGS "-g -Wall")

include_directories(./)

add_executable(main 1_6_3.cc )


# target_link_libraries(main xxxx) 


