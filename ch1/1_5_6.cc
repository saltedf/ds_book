
#include <iostream>


class IntCell {

    private:
        int *stored_value_;

    public:
        explicit IntCell(int initval)
        {
            std::cout << __FUNCTION__ <<std::endl;
            stored_value_ = new int{initval}  ;
        }

        ~IntCell() {
            std::cout << __FUNCTION__ <<std::endl;
            delete stored_value_ ;
        }

        IntCell(const IntCell& rhs)
        {
            std::cout << __FUNCTION__<< " l" <<std::endl;
            stored_value_ = new int{ *rhs.stored_value_ };
        }

        IntCell(IntCell&& rhs) {
            std::cout << __FUNCTION__<< " r" <<std::endl;
            stored_value_ = rhs.stored_value_ ;
            rhs.stored_value_ = nullptr ;
        }

        IntCell& operator=(const IntCell& rhs) {
            std::cout << __FUNCTION__<< " l" <<std::endl;

            if(&rhs != this) {//先判断是否为同一个左值
                *stored_value_ = *rhs.stored_value_ ;
            }
            return *this;
        }


        IntCell& operator=(IntCell&& rhs) {
            std::cout << __FUNCTION__<< " r" <<std::endl;
            //std::swap 接收两个变量的地址
            std::swap(stored_value_ , rhs.stored_value_ ) ;
            return *this ;
        }

        int read() const 
        {
            return *stored_value_ ;

        }

        void write(int x) 
        {
            *stored_value_ = x ;
        }

};




int main(int argc ,char** argv) {


    IntCell ic{32};

    IntCell o1{ic};

    IntCell o2{std::move(IntCell{122})  } ;

    o1 = o2 ;

    o1 =std::move( IntCell{333} );



    return 0;

}
