
#include <iostream>
#include "1_4_3.h"

int main(int argc ,char ** argv)
{


    IntCell i;

    i.write(5) ;
    std::cout << "Intcell contents: " << i.read() << std::endl;

    //只有当没有explicit时才能这样声明（不推荐）
    // IntCell err  = 22; 


    IntCell obj1{};
    IntCell obj2 {12};
    IntCell obj3;

    return 0;



}
