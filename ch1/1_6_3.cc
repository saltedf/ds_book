
#include <iostream>
#include <vector>

//正方形
class Square {
    public :
        explicit Square(double s =0.0)
            :side_(s) {}
        //边长
        double get_side() const {
            return side_;
        }
        //面积
        double get_area() const {
            return side_ * side_ ;
        }
        //周长
        double get_perimter() const {
            return 4* side_ ;
        }
        //为实现输出<<函数 提供接口
        void print(std::ostream& out = std::cout ) const {
            out << "(square " << get_side() << ")" ;
        }
        //成为Comparable的对象
        bool operator< (const Square& rhs) const {
            return side_ < rhs.side_ ;
        }

    private :
        double side_ ;

};


std::ostream& operator<<(std::ostream& out , const Square& rhs)  {
    rhs.print(out);
    return out ;
}


template <typename Comparable> 
const Comparable& FindMax(const std::vector<Comparable> & arr) {

    int max_index{};
    for(decltype(arr.size()) i = 0 ;i< arr.size() ;++i ) {
        if(arr[max_index] < arr[i] ){
            max_index = i ;
        }
    }

    return arr[max_index] ;
}


int main() {

    std::vector<Square> v = { Square{2.4} ,Square{3.0} , Square{4.2} } ;
    std::cout << FindMax(v) ;

    return 0;

}
