#include <iostream>
#include <vector>

template <typename Comparable>
const Comparable& FindMax(const std::vector<Comparable> &a ) {

    int max_index{};

    for(int i = 0;i<a.size() ;++i ){
        if(a[max_index] < a[i] ) {
            max_index = i ;
        }
    }

    return a[max_index] ;

}


int main() {


    std::vector<int> v1(10);
    std::vector<std::string> v2(2);

    v1.push_back(100);
    v2.push_back("abcd");
    v1.push_back(21);
    v2.push_back("hello");

    std::cout << FindMax(v1) << "\n" ;
    std::cout << FindMax(v2) << "\n" ;

    return 0;
}
