#include <iostream>


class IntCell {

    public:
        explicit IntCell( int initval = 0) 
            :stored_val_(initval){}

        int read() const 
        { return stored_val_;} 

        void write(int x) {
            stored_val_ = x ;
        }


    private:

        int stored_val_;

};


