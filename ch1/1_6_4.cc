#include <iostream>
#include <vector>
#include <string>
#include <cstring>


template <typename Object ,typename Comparator>
const Object& FindMax(const std::vector<Object> & arr, const Comparator& cmp) {

    decltype(arr.size()) max_index{0} ;
    decltype(arr.size()) end_index = arr.size();

    for(decltype(arr.size()) i =0 ; i< end_index ;++i) 
    {
        if(cmp(arr[max_index],arr[i]))
        {
                max_index = i ;
        }
    }
    return arr[max_index];
}


class CaseInsensitiveCompare
{
    public:
        bool operator()(const std::string& lhs, const std::string &rhs) const 
        {
            return  strcasecmp(lhs.c_str() , rhs.c_str() ) < 0;
        }
};


//非成员函数不能加const
template <typename Object>
const Object& FindMax(const std::vector<Object>& arr )   {
    //std::less 会自动调用对象的成员操作符< 
    return FindMax(arr, std::less<Object>{} ) ;
}

int main() {
    std::vector<std::string> arr = {"hello" ,"apple" ,"bird" };
    std::cout << FindMax(arr ,CaseInsensitiveCompare{}) <<"\n";

    return 0;
}

