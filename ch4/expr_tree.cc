#include <stack>
#include <string>
#include <iostream>

bool IsOperator(char ch)
{

    switch (ch)
    {
    case '+':
    case '-':
    case '*':
    case '/':
        return true;
        break;
    default:
        break;

    };

    return false;

}

template <typename T>
struct TreeNode
{
    T element;
    TreeNode* left;
    TreeNode* right;

    ~TreeNode( )
    {
        delete left;
        delete right;
    }

    void printall(int deep = 0)
    {
        std::cout << std::string(deep , ' ') << element << "\n";
        if (left != nullptr)
        {
            left->printall(deep + 1);
        }
        if (right != nullptr)
        {
            right->printall(deep + 1);
        }
    }
};



template struct TreeNode<std::string>;


struct TreeNode<std::string>* PostfixToExprTree(const std::string& str)
{
    std::stack<struct TreeNode<std::string>* > nodestack;
    int next { 0 };
    std::string token = "";

    while (next < str.size( ))
    {

        do
        {

            token.push_back(str[next]);
            next += 1;

        } while (!isspace(str[next - 1]) && next != str.size( ));

        if (isspace(str[next - 1]))
        {
            token.erase(--token.end( ));
        }



        //std::cout << token << std::endl;
        struct TreeNode<std::string>* pnode = new TreeNode<std::string> { token , nullptr , nullptr };

        if (IsOperator(token[0]))
        {
            pnode->right = nodestack.top( );
            nodestack.pop( );
            pnode->left = nodestack.top( );
            nodestack.pop( );

        }
        nodestack.push(pnode);

        token = "";

    }

    return nodestack.top( );

}



int main( )
{

    std::string postexpr = "a b + c d e + * *";
    auto ptree = PostfixToExprTree(postexpr);
    ptree->printall( );
    delete ptree;

}