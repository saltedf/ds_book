
#include "avltree.h"
#include <memory>
#include <iostream>

template <typename Comparable>
AvlTree<Comparable>::AvlNode::AvlNode()
{
    height = -1;
}

template <typename Comparable>
AvlTree<Comparable>::AvlNode::AvlNode(const Comparable &lval, const std::shared_ptr<AvlNode> &lc, const std::shared_ptr<AvlNode> &rc, int h)
    : element(lval),
      left(lc),
      right(rc),
      height(h) {}

template <typename Comparable>
AvlTree<Comparable>::AvlNode::AvlNode(Comparable &&rval, const std::shared_ptr<AvlNode> &lc, const std::shared_ptr<AvlNode> &rc, int h)
    : element(std::move(rval)),
      left(lc),
      right(rc),
      height(h) {}

template <typename Comparable>
AvlTree<Comparable>::AvlTree() {}

template <typename Comparable>
AvlTree<Comparable>::AvlTree(const AvlTree &rhs)
{
    root_ = clone(rhs.root_);
}

template <typename Comparable>
AvlTree<Comparable>::AvlTree(AvlTree &&rhs)
{
    this->root_ = rhs.root_;
    rhs.root_ = std::make_shared<AvlNode>();
}

template <typename Comparable>
std::shared_ptr<typename AvlTree<Comparable>::AvlNode>
AvlTree<Comparable>::clone(const std::shared_ptr<AvlNode> &tree)
{
    if (!tree)
    {
        return nullptr;
        //不能写： return make_shared<>(); 会产生未初始化的空节点
    }
    return std::make_shared<AvlNode>(tree->element, clone(tree->left), clone(tree->right), tree->height);
}

template <typename Comparable>
AvlTree<Comparable> &
AvlTree<Comparable>::operator=(const AvlTree &rhs)
{

    auto copy = rhs;
    std::swap(*this, copy);
    return *this;
}

template <typename Comparable>
AvlTree<Comparable> &
AvlTree<Comparable>::operator=(AvlTree &&rhs)
{
    std::swap(root_, rhs.root_);
    rhs.root_ = std::make_shared<AvlNode>();
    return *this;
}

template <typename Comparable>
const Comparable &
AvlTree<Comparable>::findMax() const
{
    return findMax(root_)->element;
}

template <typename Comparable>
const Comparable &
AvlTree<Comparable>::findMin() const
{
    return findMin(root_)->element;
}

template <typename Comparable>
bool AvlTree<Comparable>::isEmpty() const
{
    return !(root_);
}

template <typename Comparable>
bool AvlTree<Comparable>::contains(const Comparable &val) const
{
    return contains(root_, val);
}

template <typename Comparable>
void AvlTree<Comparable>::printTree() const
{
    printTree(root_);
}

template <typename Comparable>
void AvlTree<Comparable>::insert(const Comparable &lval)
{
    insert(root_, lval);
}

template <typename Comparable>
void AvlTree<Comparable>::insert(Comparable &&rval)
{
    insert(root_, std::move(rval));
}

template <typename Comparable>
void AvlTree<Comparable>::remove(const Comparable &val)
{
    remove(root_, val);
}

// pirvate:

template <typename Comparable>
int AvlTree<Comparable>::height(const std::shared_ptr<typename AvlTree<Comparable>::AvlNode> &node) const
{
    return !(node) ? -1 : node->height;
}

template <typename Comparable>
int AvlTree<Comparable>::maxHeight(int l, int r) const
{
    return l > r ? l : r;
}

template <typename Comparable>
void AvlTree<Comparable>::balance(std::shared_ptr<AvlNode> &tree)
{
    if (!tree)
    {
        return;
    }
    //处理可能为空的数据，加一层包装函数height()
    if (height(tree->left) - height(tree->right) > kAllowedImbalance)
    {
        if (height(tree->left->left) > height(tree->left->right))
        {
            rotateOnLeftSubtree(tree);
        }
        else
        {
            doubleRotateOnLeftSubtree(tree);
        }
    }

    if (height(tree->right) - height(tree->left) > kAllowedImbalance)
    {
        if (height(tree->right->right) > height(tree->right->left))
        {
            rotateOnRightSubtree(tree);
        }
        else
        {
            doubleRotateOnRightSubtree(tree);
        }
    }

    //空节点的高为-1 使得该计算方式算出的叶子节点高=0
    tree->height = maxHeight(height(tree->left), height(tree->right)) + 1;
}
/*

            (*)
           /    \
         L*      R*         
       /   \
     /       Rtree
   /
Ltree  

*/
template <typename Comparable>
void AvlTree<Comparable>::rotateOnLeftSubtree(std::shared_ptr<AvlNode> &tree)
{

    auto leftchild = tree->left;
    tree->left = leftchild->right;
    leftchild->right = tree;

    //让外面的父节点指向新的子树
    tree = leftchild;
}
/*

            (*)
           /    \
         Lc      Rc
                /  \        
            Ltree    \
                       \
                       Rtree

*/
template <typename Comparable>
void AvlTree<Comparable>::rotateOnRightSubtree(std::shared_ptr<AvlNode> &tree)
{
    auto rightchild = tree->right;
    tree->right = rightchild->left;
    rightchild->left = tree;

    tree = rightchild;
}
/*

            (*)
           /    \
         Lc      Rc         
       /   \
   Ltree     \
               \
               [*]
              /   \  
             /     \
            L       R  
*/
template <typename Comparable>
void AvlTree<Comparable>::doubleRotateOnLeftSubtree(std::shared_ptr<AvlNode> &tree)
{
    rotateOnRightSubtree(tree->left);
    rotateOnLeftSubtree(tree);
}
/*

            (*)
           /    \
         Lc      Rc
                /  \
               /   Rtree
              /     
             [*]
            /   \  
           /     \
          L       R    
                    
*/
template <typename Comparable>
void AvlTree<Comparable>::doubleRotateOnRightSubtree(std::shared_ptr<AvlNode> &tree)
{
    rotateOnLeftSubtree(tree->right);
    rotateOnRightSubtree(tree);
}

template <typename Comparable>
void AvlTree<Comparable>::insert(std::shared_ptr<AvlNode> &tree, const Comparable &lval)
{
    if (!tree)
    {
        tree = std::make_shared<AvlNode>(lval, nullptr, nullptr, 0);
    }
    else if (lval < tree->element)
    {
        insert(tree->left, lval);
    }
    else if (lval > tree->element)
    {
        insert(tree->right, lval);
    }
    else
    { //已经存在该元素
    }
    balance(tree);
}
template <typename Comparable>
void AvlTree<Comparable>::insert(std::shared_ptr<AvlNode> &tree, Comparable &&rval)
{
    if (!tree)
    {
        tree = std::make_shared<AvlNode>(std::move(rval), nullptr, nullptr, 0);
    }
    else if (rval < tree->element)
    {
        insert(tree->left, std::move(rval));
    }
    else if (rval > tree->element)
    {
        insert(tree->right, std::move(rval));
    }
    else
    { //已经存在该元素
    }
    balance(tree);
}

template <typename Comparable>
void AvlTree<Comparable>::remove(std::shared_ptr<AvlNode> &tree, const Comparable &val)
{
    if (!tree)
    {
        return; //不存在该元素
    }
    else if (tree->element > val)
    {
        remove(tree->left, val);
    }
    else if (val > tree->element)
    {
        remove(tree->right, val);
    }
    else if (tree->left && tree->right)
    {
        auto next = findMin(tree->right);
        tree->element = next->element;
        remove(tree->right, next->element);
    }
    else
    {
        auto child = !(tree->left) ? tree->left : tree->right;
        tree = child;
    }
}

template <typename Comparable>
std::shared_ptr<typename AvlTree<Comparable>::AvlNode> AvlTree<Comparable>::findMax(const std::shared_ptr<AvlNode> &tree) const
{
    auto maxnode = tree;
    while (!maxnode->right)
    {
        maxnode = maxnode->right;
    }
    return maxnode;
}

template <typename Comparable>
std::shared_ptr<typename AvlTree<Comparable>::AvlNode> AvlTree<Comparable>::findMin(const std::shared_ptr<AvlNode> &tree) const
{
    auto minnode = tree;
    while (!minnode->left)
    {
        minnode = minnode->left;
    }
    return minnode;
}

template <typename Comparable>
bool AvlTree<Comparable>::contains(const std::shared_ptr<AvlNode> &tree, const Comparable &val) const
{
    if (!tree)
    {
        return false;
    }
    else if (val < tree->element)
    {
        contains(tree->left, val);
    }
    else if (val > tree->element)
    {
        contains(tree->right, val);
    }
    return true;
}

template <typename Comparable>
void AvlTree<Comparable>::printTree(const std::shared_ptr<AvlNode> &tree) const
{
    if (!tree)
    {
        return;
    }
    printTree(tree->left);
    std::cout << tree->element << " ";
    printTree(tree->right);
}

/////////////////

template class AvlTree<int>;