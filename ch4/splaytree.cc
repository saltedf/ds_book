#include <memory>
#include <utility>
#include <iostream>
#include <algorithm>

template <typename T>
class SplayTree
{
    struct node;

public:
    typedef T value_type;
    typedef std::shared_ptr<node> node_ptr;

    bool contain(const value_type &val)
    {
        splay2(val, root_);
        if (!root_)
        {
            return false;
        }
        return root_->element == val;
    }
    void remove(const value_type &val)
    {
        node_ptr newroot;
        if (!contain(val))
        {
            return;
        }
        if (!root_->left)
        {
            newroot = root_->right;
        }
        else
        {
            newroot = root_->left;
            splay2(val, newroot);
            newroot->right = root_->right;
        }
        root_ = newroot;
    }
    void insert(const value_type &val)
    {
        auto newnode = std::make_shared<node>(val, nullptr, nullptr);
        splay2(newnode->element, root_);
        if (!root_)
        {
            root_ = newnode;
            return;
        }
        if (newnode->element < root_->element)
        {
            newnode->right = root_;
            newnode->left = root_->left;
            root_->left = nullptr;
            root_ = newnode;
        }
        else if (newnode->element > root_->element)
        {
            newnode->left = root_;
            newnode->right = root_->right;
            root_->right = nullptr;
            root_ = newnode;
        }
    }
    void insert(value_type &&val)
    {

        auto newnode = std::make_shared<node>(std::move(val), nullptr, nullptr);
        splay2(newnode->element, root_);
        if (!root_)
        {
            root_ = newnode;
            return;
        }
        if (newnode->element < root_->element)
        {
            newnode->right = root_;
            newnode->left = root_->left;
            root_->left = nullptr;
            root_ = newnode;
        }
        else if (newnode->element > root_->element)
        {
            newnode->left = root_;
            newnode->right = root_->right;
            root_->right = nullptr;
            root_ = newnode;
        }
    }

    void printTree()
    {
        printTree(root_);
    }

private:
    struct node
    {
        value_type element;
        node_ptr left;
        node_ptr right;

        node()
        {
            left = nullptr;
            right = nullptr;
        }

        node(const value_type &val, const node_ptr &lc, const node_ptr &rc)
            : element(val),
              left(lc),
              right(rc)
        {
        }
        node(value_type &&val, const node_ptr &lc, const node_ptr &rc)
            : element(std::move(val)),
              left(lc),
              right(rc) {}
    };

    node_ptr root_;

    bool contain(const value_type &val, node_ptr &tree)
    {
        splay2(val, root_);
        if (!root_)
        {
            return false;
        }
        return root_->element == val;
    }

    void printTree(const node_ptr &root)
    {
        if (!root)
        {
            return;
        }
        printTree(root->left);
        std::cout << root->element << " ";
        printTree(root->right);
    }

    void splay2(const value_type &val, node_ptr &t)
    {
        if (!t)
            return;

        node_ptr n = std::make_shared<node>();
        node_ptr l, r, y;

        n->left = nullptr;
        n->right = nullptr;
        l = r = n;

        while (1)
        {
            if (val < t->element)
            {
                if (!t->left)
                    break;

                if (val < t->left->element)
                {
                    y = t->left;
                    t->left = y->right;
                    y->right = t;
                    t = y;
                    if (!t->left)
                        break;
                }
                r->left = t;
                r = t;
                t = t->left;
            }
            else if (val > t->element)
            {
                if (!t->right)
                    break;
                if (val > t->right->element)
                {
                    y = t->right;
                    t->right = y->left;
                    y->left = t;
                    t = y;
                    if (!t->right)
                        break;
                }
                l->right = t;
                l = t;
                t = t->right;
            }
            else
            {
                break;
            }
        }
        l->right = r->left = nullptr;
        //
        l->right = t->left;
        r->left = t->right;
        t->left = n->right;
        t->right = n->left;
    }
};

int main()
{

    SplayTree<int> t;

    t.insert(12);

    t.insert(3);

    t.insert(43);
    t.insert(4);
    t.printTree();
    std::cout << "\n";

    t.insert(5);
    t.insert(6);
    t.insert(7);

    t.remove(4);

    t.insert(45);
    t.insert(34);
    // t.insert(89);
    t.printTree();
    std::cout << "\n";
    std::cout << t.contain(43);
    // t.printTree();
}