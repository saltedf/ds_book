#ifndef DSBOOK_BSTREE_H
#define DSBOOK_BSTREE_H

#include <utility>
#include<iostream>
#include <memory>

template<typename Comparable>
class BinarySearchTree
{
public:
    BinarySearchTree() {}
    //~BinarySearchTree();

    BinarySearchTree(const BinarySearchTree& rhs);
    BinarySearchTree(BinarySearchTree&& rhs);

    BinarySearchTree& operator=(const BinarySearchTree& lval);
    BinarySearchTree& operator=(BinarySearchTree&& rval);


    bool isEmpty() const;

    const Comparable& findMin() const;
    const Comparable& findMax() const;

    bool contains(const Comparable& lval) const;

    //按顺序打印出树中元素
    void printTree(std::ostream& out = std::cout) const;

    //void makeEmpty();

    //设置类数据的函数才由左右值参数之分
    void insert(const Comparable& lval);
    void insert(Comparable&& rval);

    void remove(const Comparable& lval);


private:

    struct BinaryNode
    {
        Comparable element;
        std::shared_ptr<BinaryNode> left;
        std::shared_ptr<BinaryNode> right;
        BinaryNode();
        BinaryNode(const Comparable& val , const std::shared_ptr<BinaryNode>& lc , const std::shared_ptr<BinaryNode>& rc);
        BinaryNode(Comparable&& val , const std::shared_ptr<BinaryNode>& lc , const std::shared_ptr<BinaryNode>& rc);
    };

    std::shared_ptr<BinaryNode> root_;

private:

    void insert(const Comparable& lval , std::shared_ptr<BinaryNode>& t);
    void insert(Comparable&& lval , std::shared_ptr<BinaryNode>& t);

    void remove(const Comparable& lval , std::shared_ptr<BinaryNode>& t);

    std::shared_ptr<BinaryNode> findMin(const std::shared_ptr<BinaryNode>& t) const;
    std::shared_ptr<BinaryNode> findMax(const std::shared_ptr<BinaryNode>& t) const;


    bool contains(const Comparable& val , const std::shared_ptr<BinaryNode>& tree) const;

    //void makeEmpty(const std::shared_ptr<BinaryNode>& tree);

    void printTree(const std::shared_ptr<BinaryNode>& tree , std::ostream& out) const;
    std::shared_ptr<BinaryNode> clone(const std::shared_ptr<BinaryNode>& tree) const;


};

#endif // DSBOOK_BSTREE_H 
