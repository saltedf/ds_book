
#include <iostream>
#include "avltree.h"

int main()
{

    AvlTree<int> t;
    t.insert(100);
    t.insert(34);
    t.insert(32);
    t.insert(78);
    t.insert(3);

    t.insert(12);
    t.insert(43);
    t.insert(56);
    t.insert(57);
    t.insert(60);
    t.insert(62);
    t.insert(63);
    //t.remove(3);

    t.printTree();
    std::cout << "\n";

    auto t2 = t;
    t2.printTree();
    std::cout << "\n";

    {
        AvlTree<int> tmp;

        tmp.insert(12);
        tmp.insert(43);
        tmp.insert(56);
        tmp.insert(57);
        tmp.insert(60);
        tmp.insert(62);
        tmp.insert(63);

        t2 = std::move(tmp);
    }

    t2.printTree();
    std::cout << "\n";

    return 0;
}