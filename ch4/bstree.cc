#include "bstree.h"
#include <iostream>

template<typename Comparable>
BinarySearchTree<Comparable>::BinaryNode::BinaryNode() {}

template<typename Comparable>
BinarySearchTree<Comparable>::BinaryNode::BinaryNode(const Comparable& val , const std::shared_ptr<BinaryNode>& lc , const std::shared_ptr<BinaryNode>& rc)
    :element(val) ,
    left(lc) ,
    right(rc)
{}


template<typename Comparable>
BinarySearchTree<Comparable>::BinaryNode::BinaryNode(Comparable&& val , const std::shared_ptr<BinaryNode>& lc , const std::shared_ptr<BinaryNode>& rc)
    :element(std::move(val)) ,
    left(lc) ,
    right(rc)
{}

template<typename Comparable>
bool BinarySearchTree<Comparable>::isEmpty() const
{
    return !(root_);
}

template<typename Comparable>
BinarySearchTree<Comparable>::BinarySearchTree(const BinarySearchTree& rhs)
{
    root_ = clone(rhs.root_);

}

template<typename Comparable >
BinarySearchTree<Comparable>& BinarySearchTree<Comparable>::operator=(const BinarySearchTree& rhs)
{
    BinarySearchTree<Comparable> copy = rhs;
    std::swap(*this , copy);
    return *this;
}

template<typename Comparable>
BinarySearchTree<Comparable>::BinarySearchTree(BinarySearchTree&& rhs)
{
    this->root_ = rhs.root_;
    rhs.root_ = std::make_shared<typename BinarySearchTree<Comparable>::BinaryNode >();
}
template<typename Comparable >
BinarySearchTree<Comparable>& BinarySearchTree<Comparable>::operator=(BinarySearchTree&& rhs)
{

    std::swap(root_ , rhs.root_);
    return *this;

}

template<typename Comparable >
std::shared_ptr<typename BinarySearchTree<Comparable>::BinaryNode> BinarySearchTree<Comparable>::clone(const std::shared_ptr<BinaryNode>& tree) const
{
    if (!tree)
    {
        return std::shared_ptr<BinaryNode>();
    }
    else
    {
        return std::make_shared<BinaryNode>(tree->element , clone(tree->left) , clone(tree->right));
    }
}

template<typename Comparable>
void BinarySearchTree<Comparable>::printTree(std::ostream& out)const
{
    printTree(root_ , out);
}

template<typename Comparable>
void BinarySearchTree<Comparable>::printTree(const std::shared_ptr<typename BinarySearchTree<Comparable>::BinaryNode>& tree , std::ostream& out)const
{
    if (!tree)
    {
        return;
    }
    printTree(tree->left , out);
    std::cout << tree->element << " ";
    printTree(tree->right , out);

}

template<typename Comparable>
bool BinarySearchTree<Comparable>::contains(const Comparable& val) const
{
    return contains(val , root_);
}


template<typename Comparable>
void BinarySearchTree<Comparable>::remove(const Comparable& val)
{
    remove(val , root_);
}


template<typename Comparable>
void BinarySearchTree<Comparable>::insert(const Comparable& val)
{
    insert(val , root_);
}

template<typename Comparable>
void BinarySearchTree<Comparable>::insert(Comparable&& rval)
{
    insert(std::move(rval) , root_);
}



template<typename Comparable>
bool BinarySearchTree<Comparable>::contains(const Comparable& val , const std::shared_ptr<BinaryNode>& tree) const
{
    if (!tree)
    {
        return false;
    }
    else if (val < tree->element)
    {
        return contains(val , tree->left);
    }
    else if (val > tree->element)
    {
        return contains(val , tree->right);
    }
    else
    {
        return true;
    }

}


template<typename Comparable>
std::shared_ptr<typename BinarySearchTree<Comparable>::BinaryNode > BinarySearchTree<Comparable>::findMin(const std::shared_ptr<BinaryNode>& tree) const
{
    if (!tree)
    {
        return std::shared_ptr<BinaryNode>();
    }
    else if (!tree->left)
    {
        return tree;
    }
    else
    {
        return findMin(tree->left);
    }

}

template<typename Comparable>
std::shared_ptr<typename BinarySearchTree<Comparable>::BinaryNode >  BinarySearchTree<Comparable>::findMax(const std::shared_ptr<BinaryNode>& tree) const
{

    if (!tree)
    {
        return std::shared_ptr<BinaryNode>();
    }

    std::shared_ptr<BinaryNode> finded { tree };

    while (!finded->right)
    {
        finded = finded->right;
    }
    return finded;
}



template<typename Comparable>
void BinarySearchTree<Comparable>::insert(const Comparable& lval , std::shared_ptr<BinaryNode>& tree)
{
    if (!tree)
    {
        tree = std::make_shared<BinaryNode>(lval , nullptr , nullptr);
    }
    else if (lval < tree->element)
    {
        insert(lval , tree->left);

    }
    else if (lval > tree->element)
    {
        insert(lval , tree->right);
    }


}

template<typename Comparable>
void BinarySearchTree<Comparable>::insert(Comparable&& rval , std::shared_ptr<BinaryNode>& tree)
{
    if (!tree)
    {
        tree = std::make_shared<BinaryNode>(std::move(rval) , nullptr , nullptr);
    }
    else if (rval < tree->element)
    {
        insert(std::move(rval) , tree->left);

    }
    else if (rval > tree->element)
    {
        insert(std::move(rval) , tree->right);
    }

}

template<typename Comparable>
void BinarySearchTree<Comparable>::remove(const Comparable& val , std::shared_ptr<BinaryNode>& tree)
{

    if (!tree)
    {
        return;
    }
    else if (val < tree->element)
    {
        remove(val , tree->left);
    }
    else if (val > tree->element)
    {
        remove(val , tree->right);
    }
    else if (tree->left && tree->right)
    {
        auto leftmost = findMin(tree->right);
        tree->element = leftmost->element;

        remove(leftmost->element , tree->right);
    }
    else
    {
        auto orphan = tree->left ? tree->left : tree->right;
        tree = orphan;
    }





}




template class BinarySearchTree<int>;
