#ifndef DSBOOK_AVLTREE_H
#define DSBOOK_AVLTREE_H

#include <utility>
#include <memory>

// 何时发生旋转 :插入时破坏了左右子树高度差<=1
// 如何旋转

template <typename Comparable>
class AvlTree
{
public:
    AvlTree();

    AvlTree(const AvlTree &rhs);
    AvlTree(AvlTree &&rhs);

    AvlTree &operator=(const AvlTree &rhs);
    AvlTree &operator=(AvlTree &&rhs);

    const Comparable &findMax() const;
    const Comparable &findMin() const;

    bool isEmpty() const;
    bool contains(const Comparable &val) const;

    void printTree() const;

    void insert(const Comparable &lval);
    void insert(Comparable &&rval);

    void remove(const Comparable &val);

private:
    static const int kAllowedImbalance = 1;

    struct AvlNode
    {
        Comparable element;
        std::shared_ptr<AvlNode> left;
        std::shared_ptr<AvlNode> right;

        int height; //到子树树叶的最长距离

        AvlNode();
        AvlNode(const Comparable &lval, const std::shared_ptr<AvlNode> &lc, const std::shared_ptr<AvlNode> &rc, int h);
        AvlNode(Comparable &&rval, const std::shared_ptr<AvlNode> &lc, const std::shared_ptr<AvlNode> &rc, int h);
    };

    std::shared_ptr<AvlNode> root_;

private:
    std::shared_ptr<AvlNode> findMax(const std::shared_ptr<AvlNode> &tree) const;
    std::shared_ptr<AvlNode> findMin(const std::shared_ptr<AvlNode> &tree) const;

    bool contains(const std::shared_ptr<AvlNode> &tree, const Comparable &val) const;

    void printTree(const std::shared_ptr<AvlNode> &tree) const;

    void insert(std::shared_ptr<AvlNode> &tree, const Comparable &lval);
    void insert(std::shared_ptr<AvlNode> &tree, Comparable &&rval);

    void remove(std::shared_ptr<AvlNode> &tree, const Comparable &val);

    std::shared_ptr<AvlNode> clone(const std::shared_ptr<AvlNode> &tree);

    void balance(std::shared_ptr<AvlNode> &tree);

    void rotateOnLeftSubtree(std::shared_ptr<AvlNode> &tree);
    void rotateOnRightSubtree(std::shared_ptr<AvlNode> &tree);

    void doubleRotateOnLeftSubtree(std::shared_ptr<AvlNode> &tree);
    void doubleRotateOnRightSubtree(std::shared_ptr<AvlNode> &tree);

    int height(const std::shared_ptr<AvlNode> &node) const;

    int maxHeight(int l, int r) const;
};

#endif // DSBOOK_AVLTREE_H
