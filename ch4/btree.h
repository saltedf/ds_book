#pragma once

#include <memory>

template <typename Item, typename Key>
class Btree
{
public:
private:
    std::shared_ptr<Node> link_;

    static int M = 4;

    struct Entry
    {
        using nodeptr = std::shared_ptr<Node>;

        Key key;
        Item item;
        nodeptr next;
    };

    struct Node
    {

        int m;
        Entry<Item, Key> b[M];
        Node()
            : m(0) {}
    };
};