#ifndef DSBOOK_VECTER_H
#define DSBOOK_VECTER_H


template <typename Object>
class Vector
{
public:

    typedef Object* iterator;
    typedef const Object* const_iterator;

    static const int kSpareCapacity = 16;

    explicit Vector(int initsize = 0);

    ~Vector( );

    Vector(const Vector& rhs);
    Vector& operator=(const Vector& rhs);

    Vector(Vector&& rhs);
    Vector& operator=(Vector&& rhs);

    iterator begin( );
    const_iterator begin( ) const;

    iterator end( );
    const_iterator end( ) const;

    Object& operator[](int index);
    const Object& operator[] (int index) const;

    bool empty( ) const;
    int size( ) const;
    int capacity( ) const;

    void resize(int new_size);
    void reserve(int new_capacity);

    void push_back(const Object& x);
    void push_back(Object&& x);

    void pop_back( );

    const Object& front( ) const;
    const Object& back( ) const;

    Object& front( );
    Object& back( );

private:
    int size_;
    int capacity_;
    Object* data_;


};

#endif //DSBOOK_VECTER_H 
