#include "vector.h"


#include <iostream>


template <typename Object>
void print_vector(const Vector<Object> &vec );

int main() 
{

    Vector<int> a{3} ;

    a.push_back(23);
    a.push_back(34);
    a.push_back(45);
    a.push_back(56);

    a.push_back(99);

    print_vector(a);
    std::cout << "size: "<< a.size() << "\n" ;
    

    a.pop_back();

    std::cout <<a.front()<< "  " << a.back() << "\n" ;
    






    Vector<int> b = a;

    print_vector(b);
    {

        Vector<int> tmp{2};
    tmp.push_back(23);
    tmp.push_back(34);
    tmp.push_back(45);

    b = std::move(tmp) ;


    }

    print_vector(b);


    a = b;
    print_vector(a);










}

template <typename Object>
void print_vector(const Vector<Object>& vec )
{

    for(auto iter = vec.begin() ; iter != vec.end() ;iter++ )
    {
        std::cout << *iter << " " ;
    }
    std::cout << "\n" ;
}
