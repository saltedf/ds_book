#include "vector.h"
#include <algorithm>


template <typename Object>
Vector<Object>::Vector(int initsize)
    :size_(0) ,
    capacity_(initsize + kSpareCapacity)
{
    data_ = new Object[capacity_];
}

template <typename Object>
Vector<Object>::~Vector( )
{
    delete[ ] data_;
}

template <typename Object >
void Vector<Object>::reserve(int new_capacity)
{

    if (new_capacity < size_)
    {
        return;
    }

    auto new_data = new Object[new_capacity];
    for (int i = 0;i < size_; ++i)
    {
        new_data[i] = std::move(data_[i]);
    }
    std::swap(new_data , data_);
    capacity_ = new_capacity;

    delete[ ] new_data;

}

template<typename Object>
void Vector<Object>::resize(int new_size)
{
    if (new_size > capacity_)
    {

        reserve(2 * new_size + 1);
    }
    size_ = new_size;
}


template <typename Object>
Vector<Object>::Vector(const Vector<Object>& rhs)
    : size_(rhs.size_) ,
    capacity_(rhs.capacity_) ,
    data_(new Object[capacity_])
{
    for (int i = 0;i < size_;++i)
    {
        data_[i] = rhs.data_[i];
    }
}

template <typename Object>
Vector<Object>& Vector<Object>::operator=(const Vector<Object>& rhs)
{
    Vector<Object> tmp = rhs;

    std::swap(*this , tmp);

    return *this;
}

template <typename Object>
Vector<Object>::Vector(Vector<Object>&& rhs)
    :size_(rhs.size_) ,
    capacity_(rhs.capacity_) ,
    data_(rhs.data_)
{

    rhs.data_ = nullptr;
    rhs.size_ = 0;
    rhs.capacity_ = 0;

}

template <typename Object>
Vector<Object>& Vector<Object>::operator=(Vector<Object>&& rhs)
{

    std::swap(data_ , rhs.data_);
    std::swap(capacity_ , rhs.capacity_);

    std::swap(size_ , rhs.size_);
    return *this;
}



template<typename Object>
Object& Vector<Object>::operator[](int index)
{
    return data_[index];
}


template<typename Object>
const Object& Vector<Object>::operator[](int index) const
{
    return data_[index];
}


template <typename Object>
typename Vector<Object>::iterator Vector<Object>::begin( )
{

    return &data_[0];
}



template <typename Object>
typename Vector<Object>::const_iterator Vector<Object>::begin( ) const
{

    return &data_[0];
}


template <typename Object>
typename Vector<Object>::iterator Vector<Object>::end( )
{
    return &data_[size_];
}



template <typename Object>
typename Vector<Object>::const_iterator Vector<Object>::end( ) const
{

    return &data_[size_];
}



template<typename Object>
bool Vector<Object>::empty( ) const
{
    return size_ == 0;
}

template<typename Object>
int Vector<Object>::size( ) const
{
    return size_;
}

template <typename Object>
int Vector<Object>::capacity( ) const
{
    return capacity_;
}


template<typename Object>
const Object& Vector<Object>::front( ) const
{
    return data_[0];
}

template<typename Object>
const Object& Vector<Object>::back( ) const
{
    return data_[size_ - 1];
}


template<typename Object>
Object& Vector<Object>::front( )
{
    return data_[0];
}

template<typename Object>
Object& Vector<Object>::back( )
{
    return data_[size_ - 1];
}


template<typename Object>
void Vector<Object>::pop_back( )
{
    if (size_ != 0)
    {
        size_ -= 1;
    }
}




template<typename Object>
void Vector<Object>::push_back(const Object& x)
{
    if (size_ == capacity_)
    {
        reserve(capacity_ * 2 + 1);
    }
    data_[size_] = x;
    size_ += 1;
}

template<typename Object>
void Vector<Object>::push_back(Object&& x)
{
    if (size_ == capacity_)
    {
        reserve(capacity_ * 2 + 1);
    }
    data_[size_] = std::move(x);
    size_ += 1;
}




template class Vector<char>;