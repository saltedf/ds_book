
#include "list.h"
#include <iostream>

template <typename T>
void print_list(const List<T> &list) 
{
    auto iter = list.begin();
    auto end = list.end();
    for(;iter!=end ;++iter )
    {
        std::cout << *iter << " " ;
    }

    std::cout <<"\n";
}


int main( )
{
    List<int> a;
    a.push_back(12);
    a.push_back(23);
    a.push_back(34);
    a.push_back(45);

    a.push_front(199) ;

    print_list(a);


    List<int> b{a} ;
    b.pop_front();
    b.pop_back();
    print_list(b);

    {
        List<int> c{a};
        b = std::move(c);
    }

    print_list(b);

    



}
