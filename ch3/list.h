#ifndef DSBOOK_LIST_H
#define DSBOOK_LIST_H

#include <utility>


template <typename Object>
class List
{

    struct Node
    {
        Object data;
        Node* prev;
        Node* next;

        Node( ) { }
        Node(const Object& d , Node* p , Node* n)
            :data(d) ,
            prev(p) ,
            next(n)
        { }

        Node(const Node& rhs)
            : data(rhs.data) ,
            prev(rhs.prev) ,
            next(rhs.next)
        { }


        Node(Node&& rhs)
            : data(std::move(rhs.data)) ,
            prev(rhs.prev) ,
            next(rhs.next)
        { }
    };

public:

    class const_iterator
    {
    public:

        const_iterator( )
            :current_(nullptr)
        { }
        //const类型迭代器，因此取出的对象也是const引用
        const Object& operator*( ) const
        {
            return retrieve( );
        }

        //前缀++ ，先自增，再返回自己本身
        const_iterator& operator++( )
        {
            current_ = current_->next;
            return *this;
        }
        //后缀++ 匿名单参数。既要自增，又要把旧的返回。因此返回的是副本（而非引用）
        const_iterator operator++(int)
        {
            auto old = *this;
            current_ = current_->next;
            return old;

        }

        const_iterator& operator--( )
        {
            current_ = current_->prev;
            return *this;
        }

        const_iterator  operator--(int)
        {
            auto old = *this;
            current_ = current_->prev;
            return old;
        }

        bool operator==(const const_iterator& rhs) const
        {
            return current_ == rhs.current_;
        }

        bool operator!=(const const_iterator& rhs) const
        {
            return !(*this == rhs);
        }



    protected:

        Node* current_;

        Object& retrieve( ) const
        {
            return current_->data;
        }

        //既不能对外暴露，同时也要对子类公开。并且要让List可以调用它
        const_iterator(Node* node)
            :current_(node)
        { }
        friend class List<Object>;




    };

    class iterator : public const_iterator
    {
    public:
        iterator( ) { }
        Object& operator*( )
        {
            return const_iterator::retrieve( );
        }
        const Object& operator*( ) const
        {
            return const_iterator ::operator*( );
        }

        iterator& operator++( )
        {
            this->current_ = this->current_->next;
            return *this;
        }

        iterator operator++(int)
        {
            auto old = *this;
            this->current_ = this->current_->next;
            return old;
        }
        iterator& operator--( )
        {
            this->current_ = this->current_->prev;
            return *this;
        }

        iterator  operator--(int)
        {
            auto old = *this;
            this->current_ = this->current_->prev;
            return old;
        }

    protected:

        iterator(Node* node)
            :const_iterator(node)
        { }

        friend class List<Object>;



    };








public:

    List( );

    ~List( );

    List(const List& rhs);
    List(List&& rhs);


    List& operator=(const List& rhs);
    List& operator=(List&& rhs);

    iterator begin( );
    const_iterator begin( ) const;

    iterator end( );
    const_iterator end( ) const;

    int size( ) const;
    bool empty( ) const;

    void clear( );

    Object& front( );
    const Object& front( ) const;

    Object& back( );
    const Object& back( ) const;

    void push_back(const Object& lval);
    void push_back(Object&& rval);

    void push_front(const Object& lval);
    void push_front(Object&& rval);

    void pop_back( );
    void pop_front( );

    iterator erase(iterator iter);
    iterator erase(iterator from , iterator until);

    //在iter之前插入
    iterator insert(iterator iter , const Object& lval);
    iterator insert(iterator iter , Object&& rval);



private:

    void init( );
    // struct Node;


private:

    int size_;
    Node* head_;
    Node* tail_;


};


#endif // DSBOOK_LIST_H
