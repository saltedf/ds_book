
#include "list.h"
#include <algorithm>

// template <typename Object>
// class List<Object>::const_iterator
// {
// public:

//     const_iterator( )
//         :current_(nullptr)
//     { }
//     //const类型迭代器，因此取出的对象也是const引用
//     const Object& operator*( ) const
//     {
//         return retrieve( );
//     }

//     //前缀++ ，先自增，再返回自己本身
//     const_iterator& operator++( )
//     {
//         current_ = current_->next;
//         return *this;
//     }
//     //后缀++ 匿名单参数。既要自增，又要把旧的返回。因此返回的是副本（而非引用）
//     const_iterator operator++(int)
//     {
//         auto old = *this;
//         current_ = current_->next;
//         return old;

//     }

//     const_iterator& operator--( )
//     {
//         current_ = current_->prev;
//         return *this;
//     }

//     const_iterator  operator--(int)
//     {
//         auto old = *this;
//         current_ = current_->prev;
//         return old;
//     }

//     bool operator==(const List& rhs) const
//     {
//         return current_ == rhs.current_;
//     }
//     bool operator!=(const List& rhs) const
//     {
//         return !(*this == rhs);
//     }



// protected:

//     Node* current_;

//     Object& retrieve( ) const
//     {
//         return current_->data;
//     }

//     //既不能对外暴露，同时也要对子类公开。并且要让List可以调用它
//     const_iterator(Node* node)
//         :current_(node)
//     { }
//     friend class List<Object>;




// };

// template<typename Object>
// class  List<Object>::iterator : public  List<Object>::const_iterator



template< typename Object>
List<Object>::List( )
{
    init( );
}


template <typename Object>
List<Object>::~List( )
{
    clear( );
    delete head_;
    delete tail_;
}

template <typename Object>
List<Object>::List(const List<Object>& rhs)
{

    init();
    for (auto& e : rhs)
    {
        push_back(e);
    }



}
template <typename Object>
List<Object>& List<Object>::operator=(const List<Object>& rhs)
{
    clear( );

    for (auto& e : rhs)
    {
        push_back(e);
    }
    return *this;

}
template <typename Object>
List<Object>::List(List<Object>&& rhs)
{
    init();

    for (auto& e : rhs)
    {
        push_back(std::move(e));
    }



}
template <typename Object>
List<Object>& List<Object>::operator=(List<Object>&& rhs)
{
    clear( );

    for (auto& e : rhs)
    {
        push_back(std::move(e));
    }

    return *this;
}


template< typename Object>
void List<Object> ::init( )
{
    head_ = new Node {};
    tail_ = new Node {};
    head_->prev = nullptr;
    head_->next = tail_;
    tail_->prev = head_;
    tail_->next = nullptr;

    size_ = 0;
}




template< typename Object>
typename List<Object>::iterator List<Object>::erase(typename List<Object>::iterator iter)
{

    auto del = iter.current_;
    iterator ret { del->next };

    del->prev->next = del->next;
    del->next->prev = del->prev;
    delete del;

    size_ -= 1;

    return ret;

}


template< typename Object>
typename List<Object>::iterator  List<Object>::erase(typename List<Object>::iterator from , typename List<Object>::iterator until)
{

    while (from != until)
    {
        from = erase(from);
    }
    return until;

}

template <typename Object>
typename List<Object>::iterator List<Object>::begin( )
{
    return iterator { head_->next };
}


template <typename Object>
typename List<Object>::const_iterator  List<Object>::begin( ) const
{
    return const_iterator { head_->next };
}

template <typename Object>
typename List<Object>::iterator List<Object>::end( )
{
    return iterator { tail_ };
}


template <typename Object>
typename List<Object>::const_iterator List<Object>::end( ) const
{
    return const_iterator { tail_ };
}

template <typename Object>
void  List<Object>::clear( )
{
    erase(begin( ) , end( ));
}


template <typename Object>
int List<Object>::size( ) const
{
    return size_;
}


template <typename Object>
bool List<Object>::empty( ) const
{
    return size_ == 0;
}


template <typename Object>
Object& List<Object>::front( )
{
    return head_->next->data;
}

template <typename Object>
const Object& List<Object>::front( ) const
{
    return head_->next->data;
}
template <typename Object>
Object& List<Object>::back( )
{
    return *--end( );
}

template <typename Object>
const Object& List<Object>::back( ) const
{
    return *--end( );
}

template<typename Object>
void List<Object>::push_back(const Object& lval)
{
    insert(end( ) , lval);
}


template<typename Object>
void List<Object>::push_back(Object&& rval)
{
    insert(end( ) , std::move(rval));
}

template<typename Object>
void List<Object>::push_front(const Object& lval)
{
    insert(begin( ) , lval);
}

template<typename Object>
void List<Object>::push_front(Object&& rval)
{
    insert(begin( ) , std::move(rval));
}

template<typename Object>
void List<Object>::pop_back( )
{
    erase(--end( ));
}

template<typename Object>
void List<Object>::pop_front( )
{
    erase(begin( ));

}




template <typename Object>
typename List<Object>::iterator List<Object>::insert(typename List<Object>::iterator iter , const Object& lval)
{
    auto prevnode = iter.current_->prev;
    auto newnode = new Node { lval ,prevnode,iter.current_ };
    prevnode->next = newnode;
    iter.current_->prev = newnode;
    this->size_ += 1;
    return iterator { newnode };
}

template <typename Object>
typename List<Object>::iterator List<Object>::insert(typename List<Object>::iterator iter , Object&& rval)
{
    auto prevnode = iter.current_->prev;
    auto newnode = new Node { std::move(rval) ,prevnode,iter.current_ };
    prevnode->next = newnode;
    iter.current_->prev = newnode;
    this->size_ += 1;
    return iterator { newnode };
}




template class List<int>;

