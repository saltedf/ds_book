
#include<cstdio>
#include <list>



template <typename ListContainer>
void RemoveEveryOtherItem(ListContainer & lst) {

    auto iter = lst.begin();
    auto end = lst.end();

    while(iter != end) {
       iter = lst.erase(iter);

       if(iter!=end){
           ++iter;
       }
    }

}



int main(){

    std::list<int> lst = {32,54, 5,3,6};
    RemoveEveryOtherItem(lst);

    for(auto & e :lst) {
        printf("%d ",e) ;
    }
}

