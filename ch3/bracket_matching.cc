#include "stack.h"
#include <string>
#include <iostream>

template class Stack<char>;


bool IsOpen(char ch)
{
    switch (ch)
    {
    case '(':
    case '[':
    case '{':
    case '<':
        return true;
        break;
    default:
        break;
    };

    return false;

}

bool IsClose(char ch)
{
    switch (ch)
    {
    case ')':
    case ']':
    case '}':
    case '>':
        return true;
        break;
    default:
        break;

    };

    return false;
}


bool IsPair(char left , char right)
{
    switch (left)
    {

    case '(':
        if (right == ')')
        {
            return true;
        }
        break;

    case '[':
        if (right == ']')
        {
            return true;
        }
        break;
    case '{':
        if (right == '}')
        {
            return true;
        }
        break;
    case '<':
        if (right == '>')
        {
            return true;
        }
        break;
    default:
        break;
    };

    return false;

}




bool BracketMatch(const std::string& str)
{


    Stack<char> stack { 3 };

    for (auto& ch : str)
    {
        if (IsOpen(ch))
        {
            stack.push(ch);
        }
        else if (IsClose(ch))
        {
            char top {};
            while (!stack.empty( ))
            {
                top = stack.top( );
                stack.pop( );
                if (IsPair(top , ch))
                {
                    break;
                }

            }

            if (!IsPair(top , ch))
            {
                return false;
            }

        }

    }
    if (stack.empty( ))
    {
        return true;
    }

    return false;



}



int main(int argc , char** argv)
{

    if (argc < 2)
    {
        exit(EXIT_FAILURE);
    }

    std::string str { argv[1] };


    std::cout << BracketMatch(str) << "\n";





    return 0;

}