#include "stack.h"
#include <iostream>

template class Stack<int>;


int main(){
    Stack<int> s{3};

    s.push(12);
    s.push(32);
    s.pop();
    std::cout << s.top() << std::endl;
}

