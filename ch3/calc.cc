#include <iostream>
#include <stack>
#include <vector>
#include <string>
#include <map>


std::map<char , int> opers = {
    {'+',0},
    {'-',0},
    {'*',1},
    {'/',1},
    {'(',2},
    {')',2}
};



bool IsHigher(char cur , char top)
{
    return  opers[cur] > opers[top];
}

bool IsOperator(char ch)
{

    switch (ch)
    {
    case '+':
    case '-':
    case '*':
    case '/':
        return true;
        break;
    default:
        break;

    };

    return false;

}

std::string InfixToPostfix(const std::string& str)
{

    std::string output;
    std::stack<char> stack;

    for (auto& ch : str)
    {
        if (isalnum(ch))
        {
            output.push_back(ch);
        }
        else if (ch == '(' || IsOperator(ch))
        {
            while (!stack.empty( ) && stack.top( ) != '(' && !IsHigher(ch , stack.top( )))
            {
                output.push_back(stack.top( ));
                stack.pop( );
            }
            stack.push(ch);
        }
        else if (ch == ')')
        {
            while (!stack.empty( ))
            {
                auto top = stack.top( );
                stack.pop( );
                if (top == '(')
                {
                    break;
                }
                else
                {
                    output.push_back(top);
                }
            }
        }
    }

    while (!stack.empty( ))
    {
        output.push_back(stack.top( ));
        stack.pop( );
    }

    return output;
}







int main( )
{

    std::string expr = "a+b*c+(d*e+f)*g";
    std::cout << InfixToPostfix(expr) << "\n";

    return 0;
}


