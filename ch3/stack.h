#ifndef DSBOOK_STACK_H
#define DSBOOK_STACK_H

#include "vector.h"
#include <utility>

template <typename T>
class Stack
{

public:

    explicit Stack(int initsize)
        : data_ { initsize }
    { }

    Stack(const Stack& rhs)
        : data_(rhs.data_)
    { }

    Stack(Stack&& rhs)
        :data_(std::move(rhs.data_))
    { }



    const T& top( ) const
    {
        return data_.back( );
    }

    T& top( )
    {
        return data_.back( );
    }

    void pop( )
    {
        data_.pop_back( );
    }

    void push(const T& lval)
    {
        data_.push_back(lval);
    }

    void push(T&& rval)
    {
        data_.push_back(std::move(rval));
    }

    bool empty( ) const
    {
        return data_.empty( );
    }

    int size( ) const
    {
        return data_.size( );
    }



private:

    Vector<T> data_;


};













#endif // DSBOOK_STACK_H
