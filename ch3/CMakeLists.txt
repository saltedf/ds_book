
cmake_minimum_required(VERSION 3.10)

set(CMAKE_CXX_STANDARD 14)

project(ch3)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON )

set(CMAKE_CXX_FLAGS "-g -Wall") 

include_directories(./ )

add_executable(main expr_tree.cc ) 
